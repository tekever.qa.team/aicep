/// <reference types="cypress" />

context('Invest In Portugal Login Test', () => {

    beforeEach(() => {
        cy.visit('https://qa-myaicep.investinportugal.pt/')
    })

    it('user should be able to search', () => {
        cy.login('douglas.silva@tekever.com', 'Aicep2022')
        cy.search('lisboa')
    })
})