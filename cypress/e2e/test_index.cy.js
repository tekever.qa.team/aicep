/// <reference types="cypress" />

context('Invest In Portugal Test', () => {

    beforeEach(() => {
        cy.visit('/')
    })

    it('Search by nav bar', () => {
        cy.fixture('public/header.json').then((locators) => {
            cy.get(locators.btnSearch).click()
            cy.get(locators.inputSearch).type('lisboa')
            cy.get(locators.btnSearchSubmit).click()
        })
    })

})