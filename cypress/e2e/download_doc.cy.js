/// <reference types="cypress" />

context('Invest In Portugal Download Doc Test', () => {

    beforeEach(() => {
        cy.visit('https://qa-myaicep.investinportugal.pt/')
    })

    it('user should be able to download a document', () => {
        cy.login('douglas.silva@tekever.com', 'Aicep2022')
        cy.fixture('private/index_private.json').then((locatorsIndex) => {
            cy.get(locatorsIndex.btnUsefulDocuments).click()
        })
        cy.fixture('private/useful_documents.json').then((locatorsUsefulDocs) => {
            cy.get(locatorsUsefulDocs.btnDownloadDoc1).click()
        })
    })
})