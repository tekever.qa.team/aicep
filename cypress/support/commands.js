// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.on('uncaught:exception', (err, runnable) => {
    return false
  })

Cypress.Commands.add('login', (email, password) => {
    cy.fixture('private/login_private.json').then((locatorsLogin) => {
        cy.get(locatorsLogin.inputEmail).type(email)
        cy.get(locatorsLogin.inputPassword).type(password)
        cy.get(locatorsLogin.btnConnectMe).click()
    })
    cy.fixture('private/header_private.json').then((locatorsHeader) => {
        cy.get(locatorsHeader.btnWelcome)
        .contains('Welcome')
    })
})

Cypress.Commands.add('search', (text) => {
    cy.fixture('private/header_private.json').then((locatorsHeader) => {
        cy.wait(3000)
        cy.get(locatorsHeader.btnSearch).click()
        cy.get(locatorsHeader.inputSearch).type(text + '{enter}')
    })
})