const { defineConfig } = require("cypress");
const createBundler = require("@bahmutov/cypress-esbuild-preprocessor");
const preprocessor = require("@badeball/cypress-cucumber-preprocessor");
const createEsbuildPlugin = require("@badeball/cypress-cucumber-preprocessor/esbuild");

async function setupNodeEvents(on, config) {
  await preprocessor.addCucumberPreprocessorPlugin(on, config);

  on(
    "file:preprocessor",
    createBundler({
      plugins: [createEsbuildPlugin.default(config)],
    })
  );

  // Make sure to return the config object as it might have been modified by the plugin.
  return config;
}

module.exports = defineConfig({
  projectId: 'waef7g',
  e2e: {
    baseUrl: "https://qa.investinportugal.pt/",
    specPattern: ['cypress/e2e/features/*.feature'], //, 'cypress/e2e/stepDefinitions/*.{js,jsx,ts,tsx}', 'cypress/e2e/*.cy.{js,jsx,ts,tsx}'
    supportFile: false,
    setupNodeEvents,
  },
});